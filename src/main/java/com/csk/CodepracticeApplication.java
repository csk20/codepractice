package com.csk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodepracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodepracticeApplication.class, args);
	}

}
