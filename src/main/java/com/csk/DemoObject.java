package com.csk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DemoObject {

	
	public static void main(String[] args) {
		/*
		 * Object [] obj= { new String("string");-------compilation error at 9 new
		 * Boolean(true); new Integer(21);
		 * 
		 * };
		 * 
		 */
		ArrayList<String> array= new ArrayList<>();
		array.add("1111111");
		array.add("222222");
		array.add("333333");
		array.add(1,"44444");
		System.out.println(array);////11111,44441,22222,3333
		
		String s="JAVA";
		s=s+"rocks";
		s=s.substring(4, 8);
		s.toUpperCase();
		System.out.println(s);////////////rock
		
		
		List<Number> numlist= new ArrayList<>();
		numlist.add(Long.valueOf(10));
			//	List<Long> longlist=numlist;/////compile time error
				
		//List<Number> numlist1= new ArrayList<Integer>();
		List<?> numlist2= new ArrayList<Integer>();
	//	List<Object> numlist3= new ArrayList<Integer>();
		
		
	//	ArrayList<Integer> copy= new CopyOnWriteArrayList<Integer>();-----compilation error
		//copy.addAll(Arrays.asList(12,23,24));
		//System.out.println(copy);
	}//main
	
}//class
  