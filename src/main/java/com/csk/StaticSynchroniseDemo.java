package com.csk;

public class StaticSynchroniseDemo {
	static {
		System.out.println(" static block..");
	}
	/*
	 * synchronized(StaticSynchroniseDemo.class) {
	 * System.out.println(" synchronized block.."); }
	 */

	public static void main(String[] args) {
		System.out.println(" main method ..");
	}

}
